# Psycholinguistics Datasets

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Eye Tracking](#eye-tracking)
  - [GECO](#geco-corpus)
  - [PROVO](#provo-corpus)
  - [RSC](#russian-sentence-corpus)
  - [Potsdam](#potsdam-corpus)
  - [Barret & Søgaard](#Barrett-Søgaard-2015b)
  - [Frank et al. 2013](#franketal-2013)
  - [Frank et al. 2015](#franketal-2015)
  - [Mishra](#mishra-datasets)
  - [Dundee](#dundee-corpus)
- [SPR](#spr)
  - [Natural Stories](#natural-stories)
- [fMRI](#fmri)
  - [Shain et al.](#shainetal-2020)
- [Additional Resources](#additional-resources)

## Eye tracking

### GECO Corpus

English-Dutch bilingual corpus. Eye tracking data from 17 participants. 4,934 sentences. Reading of a one complete novel. [Link](https://expsy.ugent.be/downloads/geco/)

Verb-Noun and Verb-Particle constructions only. [Link](https://github.com/shivaat/mwe-geco)

### Provo Corpus

English. 84 native speakers. 55 paragraphs read. 

Predictability Norm. [Link](https://osf.io/e4a2m/)

Eye Tracking Data. [Link](https://osf.io/a32be/)

Additional Eye Tracking Data [Link](https://osf.io/z3eh6/)

### Russian Sentence Corpus

Russian. 103 Participants. eye-tracking data of reading. Dependency syntax tags and dependency relations are also provided. [Link](https://osf.io/x5q2r/)

### Potsdam Corpus

German Sentences. 144 participant. 20ish sentences. Couldn't find it online. Used in [this work](https://www.sciencedirect.com/science/article/pii/S0006899306003854) and also [this other work](https://www.ling.uni-potsdam.de/~vasishth/pdfs/BostonHaleKlieglPatilVasishthJEMR08.pdf).

### Barrett & Søgaard (2015b)

English. 10 native speakers. 250 sentences. Frequency, Gaze features, POS tags and Dependency syntax. [Link](https://bitbucket.org/lowlands/release/src/master/EMNLP2015/)

### FrankEtAl (2013)

[Paper Link](https://www.sciencedirect.com/science/article/pii/S0093934X14001515#s0035)
[Download Link](https://static-content.springer.com/esm/art%3A10.3758%2Fs13428-012-0313-y/MediaObjects/13428_2012_313_MOESM1_ESM.zip)

### FrankEtAl (2015)

[Paper Link](https://link.springer.com/article/10.3758/s13428-012-0313-y#Abs1)
[Download Link](https://www.sciencedirect.com/science/article/pii/S0093934X15001182?via%3Dihub)

### Mishra Datasets

5 Eye tracking Datasets for certain papers. [Link](http://www.cfilt.iitb.ac.in/cognitive-nlp/)

### Dundee Corpus

Not online available. However, syntactic trees are online. [Link](https://www.ling.ohio-state.edu/golddundee/#kennedy)

## SPR

### Natural Stories

English. Naturalistic stories. Contains annotations, audios, frequencies, parses, and reading times. [Link](https://github.com/languageMIT/naturalstories)

## fMRI

### ShainEtAl (2020)

Post-processed fMRI time series of 78 native English Speakers. [Link](https://osf.io/eyp8q/)

## Additional Resources
- [Malsburg's List of publicly available psycholinguistics datasets](https://github.com/tmalsburg/PsychlingDatasets/wiki/A-directory-of-publicly-available-data-sets-from-psycholinguistic-studies)
- [CHILDES](http://childes-db.stanford.edu/)
- [Language Gold Mine](http://languagegoldmine.com/)
- [English Lexicon Project](https://elexicon.wustl.edu/)